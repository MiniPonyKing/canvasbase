var player = {
  width: 20,
  height: 20,
  x: 280,
  y: 180,
  dx: 20,
  dy: 20,
  isMoving: false,
  direction: 'right',
};

var leftPressed = false;
var upPressed = false;
var rightPressed = false;
var downPressed = false;
var swordPressed = false;
var lastDirection = 'd';

function onLoad() {
  canvas = document.getElementById('canvy');
  context = canvas.getContext('2d');

  document.addEventListener('keydown', onKeyDown, false);
  document.addEventListener('keyup', onKeyUp, false);

  draw();
}

function drawPlayer() {
  context.beginPath();

  context.rect(player.x, player.y, player.width, player.height);

  context.fillStyle = 'black';
  context.fill();

  context.closePath();
}

function movePlayer() {
  if (leftPressed && player.x > 0) player.x -= player.dx;
  if (upPressed && player.y > 0) player.y -= player.dy;
  if (rightPressed && player.x < canvas.width - player.width)
    player.x += player.dx;
  if (downPressed && player.y < canvas.height - player.height)
    player.y += player.dy;
}

function draw() {
  clear();

  drawGrid();

  drawPlayer();

  drawSword();

  movePlayer();

  requestAnimationFrame(draw);
}

function drawGrid() {
  context.strokeStyle = '#eee';

  for (var i = player.dy; i < canvas.height; i += player.dy) {
    context.moveTo(0, i);
    context.lineTo(canvas.width, i);
    context.stroke();
  }

  for (var i = player.dx; i < canvas.width; i += player.dx) {
    context.moveTo(i, 0);
    context.lineTo(i, canvas.height);
    context.stroke();
  }
}

function drawSword() {
  if (!swordPressed) return;

  var swordLength = 15;
  context.strokeStyle = '#000';

  context.moveTo(player.x + player.width, player.y + player.height / 2);
  context.lineTo(player.x + player.width + swordLength, player.y + player.height / 2);
  context.stroke();
}

function clear() {
  context.clearRect(0, 0, canvas.width, canvas.height);
}

function onKeyDown(e) {
  if (e.key == 'Left' || e.key == 'a') {
    lastDirection = 'a';
    leftPressed = true;
  } else if (e.key == 'Up' || e.key == 'w') {
    lastDirection = 'w';
    upPressed = true;
  } else if (e.key == 'Right' || e.key == 'd') {
    lastDirection = 'd';
    rightPressed = true;
  } else if (e.key == 'Down' || e.key == 's') {
    lastDirection = 's';
    downPressed = true;
  } else if (e.key == 'k') swordPressed = true;
}

function onKeyUp(e) {
  if (e.key == 'Left' || e.key == 'a') leftPressed = false;
  else if (e.key == 'Up' || e.key == 'w') upPressed = false;
  else if (e.key == 'Right' || e.key == 'd') rightPressed = false;
  else if (e.key == 'Down' || e.key == 's') downPressed = false;
  else if (e.key == 'k') swordPressed = false;
}
